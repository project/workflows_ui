# Editorial Interface

The custom Editorial Interface module provides a visual interface for users to manage Drupal Content Moderation workflows:

# Features:

  - Sync with the existing Workflows in the site and generate their visual representation
  - Create new workflows with states and transitions
  - Assign workflows to Entities


It can also provide:
  - Information about who created a workflow
  - The dates when a Workflow was created/updated
  - information about who updated a workflow
