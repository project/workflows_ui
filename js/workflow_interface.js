/**
 * @file
 * JavaScript for the workflow interface.
 */

jQuery(document).ready(function ($) {
    'use strict';

    let numberOfElements = 0;
    let htmlBase = 'drawingArea';
    // let htmlBase = 'edit-workflow-data';

    jsPlumb.ready(function () {
        // Load if possible.
        if($('#edit-json-data').val()) {
            loadFlowchart();
        }

        // Buttons functionality.
        $('#addTask').on('click', function (e) {
            e.preventDefault();
            // Create box and use the returned id.
            let id = addTask();
            // Create endpoints using the id.
            addConnectors(id);
          });

        $('#edit-submit').on('click', function (e) {
            e.preventDefault();
            saveFlowchart();
            $('#workflows-ui-form').submit();
        });

        // Defaults.
        jsPlumb.importDefaults({
            Anchors : [ "Left", "Right" ],
            ConnectionsDetachable   : true,
            DoNotThrowErrors  : false,
            MaxConnections : -1,
            ReattachConnections : true,
            RenderMode : "svg",
        });

        // Connectors for start and endpoint.
        let workflowConnectorStartpoint = {
            isSource: true,
            isTarget: false,
            ConnectionsDetachable: false,
            maxConnections: -1,
            anchor: "RightMiddle",
            endpoint: ["Dot", { radius: 2}]
        };

        let workflowConnectorEndpoint = {
            isSource: false,
            isTarget: true,
            ConnectionsDetachable: false,
            maxConnections: -1,
            anchor: "LeftMiddle",
            endpoint: ["Rectangle", { width: 4, height: 4}]
        };

        // Add endpoints.
        jsPlumb.addEndpoint($('.startpoint'), workflowConnectorStartpoint);
        jsPlumb.addEndpoint($('.endpoint'), workflowConnectorEndpoint);

        // Add Connectors for Draft and Published.
        addConnectors('draft');
        addConnectors('published');

        // Add draggable.
        jsPlumb.draggable($('.startpoint'));
        jsPlumb.draggable($('.endpoint'));
        // jsPlumb.draggable($('.draft'));
        // jsPlumb.draggable($('.published'));

        // Connect start to draft and published to end.
        jsPlumb.connect({
            source: 'startpoint',
            target: 'draft',
            anchors: ["RightMiddle", "LeftMiddle"],
            endpoints: [["Dot", { radius: 4}],["Rectangle", { width: 16, height: 16, cssClass: "theStart" }]]
        });
        jsPlumb.connect({
            source: 'published',
            target: 'endpoint',
            anchors: ["RightMiddle", "LeftMiddle"],
            endpoints: [["Dot", { radius: 4}],["Rectangle", { width: 8, height: 8, cssClass: "theEnd" }]]
        });

        // Remove functionality.
        $('#' + htmlBase).on("click", ".button_remove", function () {
            let parentnode = $(this)[0].parentNode.parentNode;
            // jsPlumb.detachAllConnections(parentnode);
            jsPlumb.removeAllEndpoints(parentnode);
            let choice = confirm(this.getAttribute('data-confirm'));
            if (choice == true) {
                $(parentnode).remove();
            } else {
                return false;
            }
        });
    });

    $('#custom_wf_submit').on('click', function () {
        $('#edit-submit').trigger('click');
    });

    function addTask(id) {
        if (typeof id === "undefined") {
            numberOfElements++;
            id = "taskcontainer" + numberOfElements;
        }
        $('<div class="window task node" id="' + id + '" data-nodetype="task" style="left:20px; top:110px;">').appendTo('#' + htmlBase).html($(("#taskcontainer0"))[0].innerHTML);
        return id;
    }

    // Save FlowChart as Json.
    function saveFlowchart() {
        // Define Array for Nodes.
        let nodes = []
        $(".node").each(function () {
            let $label = $(this).children().children().val();
            nodes.push({
                blockId: $(this).attr('id'),
                nodetype: $(this).attr('data-nodetype'),
                label: $label,
                positionX: parseInt($(this).css("left"), 10),
                positionY: parseInt($(this).css("top"), 10)
                // default: ($(this).find('#txt').val()!='')?$(this).find('#txt').val(): undefined

            });
        });
        // Define Array for Connections.
        let connections = [];
        $.each(jsPlumb.getConnections(), function (index, connection) {
            connections.push({
                connectionId: connection.id,
                pageSourceId: connection.sourceId,
                pageTargetId: connection.targetId
            });
        });

        let flowChart = {};
        flowChart.nodes = nodes;
        flowChart.connections = connections;
        flowChart.numberOfElements = numberOfElements;

        // Json to String.
        let flowChartJson = JSON.stringify(flowChart);

        // Add value to textarea in form.
        $('#edit-json-data').val(flowChartJson);

    }

    //loading json data to display the exact flowchart
    function loadFlowchart() {
        let flowChartJson = $('#edit-json-data').val();
        let flowChart = JSON.parse(flowChartJson);
        let nodes = flowChart.nodes;

        // Read array and repaint states.
        $.each(nodes, function (index, elem) {
            // If it is a default element.
            if (elem.nodetype === 'startpoint' || elem.nodetype === 'endpoint' || elem.nodetype === 'draft' || elem.nodetype === 'published') {
                repositionElement(elem.nodetype, elem.positionX, elem.positionY);
                // If it is draft or published.
                if (elem.nodetype === 'draft' || elem.nodetype === 'published') {
                    addConnectors(elem.blockId);
                    addLabel(elem.blockId, elem.label);
                }
            }
            // If it is an added element.
            else if (elem.nodetype === 'task') {
                // Add task.
                addTask(elem.blockId);
                // Reposition task.
                repositionElement(elem.blockId, elem.positionX, elem.positionY);
                // Add labels
                addLabel(elem.blockId, elem.label);
                // Add connectors
                addConnectors(elem.blockId);
            }
        });

        // Read array and reconnect states.
        let connections = flowChart.connections;
        $.each(connections, function (index, elem) {
            jsPlumb.connect({
                source: elem.pageSourceId,
                target: elem.pageTargetId,
                anchors: ["RightMiddle", "LeftMiddle"],
                endpoints: [["Dot", { radius: 4}],["Rectangle", { width: 8, height: 8 }]]
            });
        });

        numberOfElements = flowChart.numberOfElements;
    }

    function repositionElement(id, posX, posY) {
        $('#' + id).css('left', posX);
        $('#' + id).css('top', posY);
    }

    // Function to re-add labels.
    function addLabel(id, label) {
        $('#' + id).children().children().val(label);
    }

    // Function to add connectors.
    function addConnectors(id) {

        if ((id == 'draft') || (id == 'published')) {
            var radius = 8;
            var width = 8;
            var height = 8;
        }
        else
        {
            var adius = 4;
            var width = 16;
            var height = 16;
        }
        let workflowConnectorStart = {
            isSource: true,
            anchor: "RightMiddle",
            endpoint: ["Dot", { radius: radius, cssClass: "startAnchor"}]
        };

        let workflowConnectorEnd = {
            isTarget: true,
            anchor: "LeftMiddle",
            endpoint: ["Rectangle", { width: width, height: height, cssClass: "endAnchor" }]
        };

        // Add endpoints.
        jsPlumb.addEndpoint($('#' + id), workflowConnectorStart);
        jsPlumb.addEndpoint($('#' + id), workflowConnectorEnd);

        // Add draggable.
        jsPlumb.draggable($('#' + id));
    }
});
