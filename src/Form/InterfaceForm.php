<?php

namespace Drupal\workflows_ui\Form;

use Drupal\Core\Form\BaseFormIdInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\workflows_ui\WorkflowTool;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Component\Utility\Html;

/**
 * Implements Interface form.
 *
 * This form is used for workflow interface.
 */
class InterfaceForm extends FormBase implements BaseFormIdInterface {

  /**
   * The accreditation tool.
   *
   * @var \Drupal\workflows_ui\WorkflowTool
   */
  protected $workflowTool;

  /**
   * Constructs a organisation dashboard.
   *
   * @param \Drupal\workflows_ui\WorkflowTool $workflowTool
   *   The accredditation session.
   */
  public function __construct(WorkflowTool $workflowTool) {
    $this->workflowTool = $workflowTool;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('workflows_ui.tools')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId(): string {
    return 'config_translation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // Get name from current request.
    $name = \Drupal::service('request_stack')->getCurrentRequest()->get('name');

    if ( (string) $name == '') {
      $form['#workflow_name'] = 'add';
     } else {
      $form['#workflow_name'] = $name;
      $workflow = $this->workflowTool->getWorkflowDb($name);

    }
    // Label.
    $form['workflow_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => '',
      '#default_value' => !empty($workflow) ? Html::decodeEntities($workflow['label']) : NULL,
      '#required' => TRUE,
    ];
    // Interface.
    // $form['workflow_container'] = [
    //   '#type' =>'details',
    //   '#title' => $this->t('Flows'),
    //   '#open' => TRUE,
    // ];

    // $form['workflow_container']['workflow_data'] = [
    //   '#type' => 'textarea',
    //   '#title' => NULL,
    //   '#attributes' => ['readonly' => 'readonly', 'disabled' => 'disabled'],
    // ];

    // Json.
    $form['json_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Json'),
      '#open' => TRUE,
    ];

    $form['json_container']['json_data'] = [
      '#type' => 'textarea',
      '#title' => NULL,
      '#default_value' => !empty($workflow) ? Html::decodeEntities($workflow['jsonData']) : NULL,
      '#attributes' => ['readonly' => 'readonly', 'disabled' => 'disabled'],
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#description' => '',
    ];

  //  $form['#attached']['library'][] = 'workflows_ui/interface-style';
    
    $form['#theme'] = 'workflow_interface';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'workflows_ui_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();

    // Get name from current request.
    $name = \Drupal::service('request_stack')->getCurrentRequest()->get('name');
    $new = FALSE;
    // If we have no name, it means we are adding a new workflow.
    if (empty($name)) {
      $name = $values['workflow_name'];
      $new = TRUE;
    }
    
    $json = $values['json_data'];
    $jsonData = [];

    // If the workflow is new.
    if ($new) {
      $workflowLabel = Html::escape($values['workflow_name']); 
      $workflowJson = Html::escape($values['json_data']); 
      
      $jsonData = $this->workflowTool->parseJsonData($values['json_data']);
      $this->workflowTool->createWorkflowDb($name, $workflowLabel, $workflowJson);
      $this->workflowTool->createWorkflow($workflowLabel, $jsonData['other_states'], $jsonData['connections']);

      // redirect to list workflows
      $url = Url::fromUri('internal:/admin/config/workflow/workflows');
      $redirect = new RedirectResponse($url->toString());
      $redirect->send();

      return;
    }
    // If we have already a workflow, we edit.
    else {
          $workflow = \Drupal::entityTypeManager()->getStorage('workflow')->load($name);
          // Check if the label was modified.
  
          if (!empty($workflow)) {
            // Check the states and transitions if are changes.
            // It is needed to show error message if the state cannot be deleted
            // because it exists content in that state.       
            if ($json) {
              $oldStates = $this->workflowTool->getStatesOfWorkflow($form['#workflow_name']);
              if ($oldStates) {
                foreach ($oldStates as $key => $value) {
                  $oldStatesTransitions[$key] = $value->getTransitions();
                  $oldStatesData[] = $key;
                }
  
                $jsonParsed = $this->workflowTool->parseJsonData($values['json_data']);
                $newStates = array_keys($jsonParsed['other_states']);

                // @TODO:
                // Check case when there are less states in newStates than in oldstates.
                // $result = array_diff($newStates, $oldStatesData);
                // If there is at least a difference between new json states and old json's.
                // if (!empty($result)) {
                  // @TODO:
                  // CHeck if we need all functions.
                  $this->workflowTool->updateWorkflow($name, $values['workflow_name'], $jsonParsed['other_states'], $jsonParsed['connections']);
                  $this->workflowTool->updateWorkflowStates($name, $jsonParsed['other_states'], $jsonParsed['connections']);
                  $this->workflowTool->updateWorkflowDb($name, $values['workflow_name'], $values['json_data']);
                // }
              }
            }      
          }    
        }
      }
    }
