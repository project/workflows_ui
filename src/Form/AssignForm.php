<?php

namespace Drupal\workflows_ui\Form;

use Drupal\Component\Serialization\Json;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\workflows_ui\WorkflowTool;
use Drupal\Core\Url;

/**
 * Bulds the form for workflow assignment.
 */
class AssignForm extends FormBase implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The moderation info service.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface
   */
  protected $moderationInfo;

  /**
   * The entity type type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The workflow entity.
   *
   * @var \Drupal\workflows\Entity\Workflow
   */
  protected $workflow;

  /**
   * The workflow type plugin.
   *
   * @var \\Drupal\workflows\WorkflowTypeInterface
   */
  protected $workflowType;

  /**
   * The workflow entity.
   *
   * @var \Drupal\workflows_ui\WorkflowTool
   */
  protected $workflowTool;

  /**
   * Create an instance of ContentModerationConfigureForm.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ModerationInformationInterface $moderationInformation, EntityTypeBundleInfoInterface $entityTypeBundleInfo, WorkflowTool $workflowTool) {
    $this->entityTypeManager = $entityTypeManager;
    $this->moderationInfo = $moderationInformation;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->workflowTool = $workflowTool;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('content_moderation.moderation_information'),
      $container->get('entity_type.bundle.info'),
      $container->get('workflows_ui.tools')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'assign_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $name = ''): array {
    $this->workflow = $this->entityTypeManager->getStorage('workflow')->load($name);
    $this->workflowType = $this->workflow->getTypePlugin();
    $header = [
      'type' => $this->t('Items'),
      'operations' => $this->t('Operations'),
    ];
    $form['entity_types_container'] = [
      '#type' => 'details',
      '#title' => $this->t('This workflow applies to:'),
      '#open' => TRUE,
    ];
    $form['entity_types_container']['entity_types'] = [
      '#type' => 'table',
      '#header' => $header,
      '#empty' => $this->t('There are no entity types.'),
    ];

    $entity_types = $this->entityTypeManager->getDefinitions();
    foreach ($entity_types as $entity_type) {
      if (!$this->moderationInfo->canModerateEntitiesOfEntityType($entity_type)) {
        continue;
      }

      $selected_bundles = [];
      foreach ($this->entityTypeBundleInfo->getBundleInfo($entity_type->id()) as $bundle_id => $bundle) {
        if ($this->workflowType->appliesToEntityTypeAndBundle($entity_type->id(), $bundle_id)) {
          $selected_bundles[$bundle_id] = $bundle['label'];
        }
      };

      $selected_bundles_list = [
        '#theme' => 'item_list',
        '#items' => $selected_bundles,
        '#context' => ['list_style' => 'comma-list'],
        '#empty' => $this->t('none'),
      ];
      // Update the module's list of entities/bundles that use the workflow.
      empty($selected_bundles) ? $assignments = '' : $assignments = implode(', ', $selected_bundles);
      $this->workflowTool->updateWorkflowAssignments($name, $assignments);

      $form['entity_types_container']['entity_types'][$entity_type->id()] = [
        'type' => [
          '#type' => 'inline_template',
          '#template' => '<strong>{{ label }}</strong></br><span id="selected-{{ entity_type_id }}">{{ selected_bundles }}</span>',
          '#context' => [
            'label' => $this->t('@bundle types', ['@bundle' => $entity_type->getLabel()]),
            'entity_type_id' => $entity_type->id(),
            'selected_bundles' => $selected_bundles_list,
          ],
        ],
        'operations' => [
          '#type' => 'operations',
          '#links' => [
            'select' => [
              'title' => $this->t('Select'),
              'url' => Url::fromRoute('content_moderation.workflow_type_edit_form', [
                'workflow' => $this->workflow->id(),
                'entity_type_id' => $entity_type->id(),
              ]
              ),
              'attributes' => [
                'class' => ['use-ajax'],
                'data-dialog-type' => 'modal',
                'data-dialog-options' => Json::encode([
                  'width' => 700,
                ]),
                'data-ajax-wrapper' => 'selected-' . $entity_type->id(),
              ],
            ],
          ],
        ],
      ];
    };
    $form['assign'] = [
      '#type' => 'submit',
      '#value' => $this->t('Back to workflows'),
      '#description' => $this->t('Go back'),
      '#weight' => '2',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $to_redirect = 'internal:/ngo-branch/workflows';
    $url = Url::fromUri($to_redirect);
    $redirect = new RedirectResponse($url->toString());
    $redirect->send();
  }

}
