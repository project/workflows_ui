<?php

namespace Drupal\workflows_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\workflows\Entity\Workflow;
use Drupal\workflows_ui\WorkflowTool;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\content_moderation\ModerationInformationInterface;

/**
 * Controller implementation.
 */
class WorkflowSyncController extends ControllerBase {


  /**
   * The workflow tool.
   *
   * @var \Drupal\workflows_ui\WorkflowTool
   */
  protected $workflowTool;

  /**
   * Messenger.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The moderation info service.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface
   */
  protected $moderationInfo;

  /**
   * The entity type type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Constructs a organisation dashboard.
   *
   * @param \Drupal\workflows_ui\WorkflowTool $workflowTool
   *   The accredditation session.
   * @param Drupal\Core\Messenger\MessengerInterface $messenger
   *   This is the Messenger service.
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   * @param Drupal\content_moderation\ModerationInformationInterface $moderationInformation
   *   The moderationInformation.
   * @param Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entityTypeBundleInfo.
   */
  public function __construct(WorkflowTool $workflowTool, MessengerInterface $messenger, EntityTypeManagerInterface $entityTypeManager, ModerationInformationInterface $moderationInformation, EntityTypeBundleInfoInterface $entityTypeBundleInfo) {
    $this->workflowTool = $workflowTool;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entityTypeManager;
    $this->moderationInfo = $moderationInformation;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $workflowTool = $container->get('workflows_ui.tools');
    $messenger = $container->get('messenger');
    $entityTypeManager = $container->get('entity_type.manager');
    $moderationInfo = $container->get('content_moderation.moderation_information');
    $entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    return new static($workflowTool, $messenger, $entityTypeManager, $moderationInfo, $entityTypeBundleInfo);
  }

  /**
   * Callback to sync workflows.
   */
  public function sync(): void {
    $_workflows = $this->workflowTool->getAllWorkflows();
    // Load existing workflows from config and add them in module's registry.
    foreach (Workflow::loadMultipleByType('content_moderation') as $key => $workflow) {
      $workflowType = $workflow->getTypePlugin();
      if (!in_array($key, $_workflows)) {
        // Get all the states of a workflow.
        $states = $workflow->getTypePlugin()->getStates();
        // Get all the transitions of a workflow.
        $transitions = $workflow->getTypePlugin()->getTransitions();
        // Generate the JSON definition of the workflow.
        $json = $this->workflowTool->generateJson($states, $transitions);
        // Get all the Entity/Bundles that use the workflow.
        $entity_types = $this->entityTypeManager->getDefinitions();
        foreach ($entity_types as $entity_type) {
          foreach ($this->entityTypeBundleInfo->getBundleInfo($entity_type->id()) as $bundle_id => $bundle) {
            if ($workflowType->appliesToEntityTypeAndBundle($entity_type->id(), $bundle_id)) {
              $selected_bundles[$bundle_id] = $bundle['label'];
            }
          };
        }
        empty($selected_bundles) ? $assignments = '' : $assignments = implode(', ', $selected_bundles);
        // Save the workflow info into the module's registry (table).
        $this->workflowTool->createWorkflowDb($key, $workflow->get('label'), $json, $assignments);
        $selected_bundles = [];
      };
    };
    // Redirect back to the listing of the workflows.
    $to_redirect = 'internal:/ngo-branch/workflows';
    $url = Url::fromUri($to_redirect);
    $redirect = new RedirectResponse($url->toString());
    $redirect->send();
    $this->messenger->addMessage($this->t('Workflow information has been synced'), TRUE);

  }

  /**
   * Callback to truncate the table with workflow info.
   */
  public function clear(): void {
    // Truncate the Workflow records table.
    $this->workflowTool->clearWorkflowTable();
    // Redirect back to the listing of the workflows.
    $to_redirect = 'internal:/ngo-branch/workflows';
    $url = Url::fromUri($to_redirect);
    $redirect = new RedirectResponse($url->toString());
    $redirect->send();
    $this->messenger->addMessage($this->t('Workflow information has been cleared'), TRUE);

  }

}
