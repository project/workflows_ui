<?php

namespace Drupal\workflows_ui;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\workflows\Entity\Workflow;

/**
 * Provides tools for accreditations custom.
 */
class WorkflowTool implements ContainerInjectionInterface {

  /**
   * Drupal Entity Type Manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $dataBase;

  /**
   * Constructor.
   *
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   A config factory instance.
   * @param Drupal\Core\Database\Connection $dataBase
   *   Database.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, Connection $dataBase) {
    $this->entityTypeManager = $entityTypeManager;
    $this->dataBase = $dataBase;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('database')
    );
  }

  /**
   * Parse json Data.
   *
   * @param string $jsonData
   *   The json data from library.
   *
   * @return array
   *   Return parsed data.
   */
  public function parseJsonData($jsonData): array {
    // Why do we need this?
    $draft = FALSE;
    $published = FALSE;
    // Initialize empty arrays.
    $data = [];
    $states = [];
    $conections = [];
    $trans_defaults = [];

    if ($jsonData) {
      $decoded = Json::decode($jsonData);
      // If we have data (states).
      if ($decoded) {
        foreach ($decoded['nodes'] as $key => $value) {
          // Check for the draft state.
          // If it is a normal task then.
          if ($value['nodetype'] == 'task') {
            // Here are other states to add to the workflow.
            $states[$value['blockId']] = $value['label'];
          }
          // But if it is a default task such as Draft and Published.
          elseif ($value['nodetype'] == 'default') {
            $states[$value['blockId']] = $value['label'];
            $trans_defaults[$value['blockId']] = $value['blockId'];
          }
        }

        // If we have connections (transitions).
        if ($decoded['connections']) {
          foreach ($decoded['connections'] as $key => $value) {
            // We ignore the connections from startpoint or to endpoint.
            if ($value['pageSourceId'] != 'startpoint' && $value['pageTargetId'] != 'endpoint') {
              $conections[] = $value;
            }
          }
        }
      }
    }

    // Rewrite pageSourceId and pagTargetId with the label states.
    if ($conections) {
      foreach ($trans_defaults as $key => $value) {
        foreach ($conections as $key_t => $value_t) {
          if ($value_t['pageSourceId'] == $value) {
            $conections[$key_t]['pageSourceId'] = $key;
          }
          if ($value_t['pageTargetId'] == $value) {
            $conections[$key_t]['pageTargetId'] = $key;
          }
        }
      }
    }

    // Building the array.
    $data['other_states'] = $states;
    $data['connections'] = $conections;
    // Returning the array.
    return $data;
  }

  /**
   * Create workflow with states.
   *
   * @param string $name
   *   The machine name of the workflow.
   * @param array $states
   *   The states of the workflow.
   * @param array $transitions
   *   The transitions of the workflow.
   */
  public function createWorkflow($name, array $states, array $transitions): void {
    // Remove spaces and uppercases for id.
    $id = strtolower($name);
    $id = str_replace(' ', '_', $id);
    // End workaround to avoid deletion error.
    // Create the workflow.
    $workflow = new Workflow([
      'id' => $id,
      'label' => $name,
      'type' => 'content_moderation',
    ],
      'workflow');

    // Create states.
    foreach ($states ?: [] as $key => $value) {
      // If it is a default state (draft or published.)
      if ($key == 'draft' || $key == 'published') {
        // Edit labels.
        // Key is the blockID in frontend and value is the Label of the block.
        $workflow->getTypePlugin()->setStateLabel($key, $value);
      }
      else {
        // If it is not a default state, it means it is a new one.
        // Create new state.
        // Key is the blockID in frontend and value is the Label of the block.
        $workflow->getTypePlugin()->addState($key, $value);
      }
    }

    // Save the workflow.
    $workflow->save();

    // GET RID OF DEFAULT STATES.
    $defaultTransitions = $workflow->getTypePlugin()->getTransitions();
    if (isset($defaultTransitions) && !empty($defaultTransitions)) {
      foreach ($defaultTransitions ?: [] as $machineName => $transitionConfiguration) {
        $workflow->getTypePlugin()->deleteTransition($machineName);
      }
      // Save the workflow.
      $workflow->save();
    }

    // If we have more than 2 states, means we have at least a custom new state.
    if (count($states) > 2) {
      // Foreach transition where key is the index and value specifies the
      // source and the target.
      foreach ($transitions ?: [] as $key_t => $value_t) {
        // Create the label and machine name of transitions.
        $labelTransition = $value_t['pageSourceId'] . $value_t['pageTargetId'];
        $machineTranstion = strtolower($labelTransition);

        if (($value_t['pageSourceId'] != 'draft' &&  $value_t['pageTargetId'] != 'published') &&
        ($value_t['pageSourceId'] != 'published' &&  $value_t['pageTargetId'] != 'draft')) {
          $workflow->getTypePlugin()->addTransition($machineTranstion, $labelTransition, [$value_t['pageSourceId']], $value_t['pageTargetId']);
        }
        // This is the transition for the Draft to first state.
        if ($value_t['pageSourceId'] == 'draft' &&  $value_t['pageTargetId'] != 'published') {
          $workflow->getTypePlugin()->addTransition($machineTranstion, $labelTransition, [$value_t['pageSourceId']], $value_t['pageTargetId']);
        }
        // This is transition for the last state to published.
        if ($value_t['pageSourceId'] != 'draft' &&  $value_t['pageTargetId'] == 'published') {
          $workflow->getTypePlugin()->addTransition($machineTranstion, $labelTransition, [$value_t['pageSourceId']], $value_t['pageTargetId']);
        }
      }
    }
    $workflow->save();

  }

  /**
   * Update workflow with states.
   *
   * @param string $name
   *   The machine name of the workflow.
   * @param array $all_new_states
   *   The all new states of the workflow.
   * @param array $transitions
   *   The transitions of the workflow.
   */

  public function updateWorkflow($name, $label, array $all_new_states, array $transitions): void {
    $workflow = $this->entityTypeManager->getStorage('workflow')->load($name);
    if ($workflow) {
      $workflow->set('label', $label);
      // Create states.
      foreach ($all_new_states as $key => $value) {
        // If it is a default state (draft or published.)
        if ($key == 'draft' || $key == 'published') {
          // Edit labels.
          // Key is the blockID in frontend and value is the Label of the block.
          $workflow->getTypePlugin()->setStateLabel($key, $value);
        }
        else {
          // Check if the state exists in the system.
          // Key is the blockID in frontend and value is the Label of the block.
          if ($workflow->getTypePlugin()->hasState($key) == FALSE) {
            $workflow->getTypePlugin()->addState($key, $value);
          }
        }
      }

      foreach ($transitions ?: [] as $key_t => $value_t) {
        // Create the label and machine name of transitions.
        $labelTransition = $value_t['pageSourceId'] . $value_t['pageTargetId'];
        $machineTranstion = strtolower($labelTransition);

        // Check if the transition not exist so we will create the transition.
        if ($workflow->getTypePlugin()->hasTransitionFromStateToState($value_t['pageSourceId'], $value_t['pageTargetId']) == FALSE) {
          if (($value_t['pageSourceId'] != 'draft' &&  $value_t['pageTargetId'] != 'published') &&
          ($value_t['pageSourceId'] != 'published' &&  $value_t['pageTargetId'] != 'draft')) {

            $workflow->getTypePlugin()->addTransition($machineTranstion, $labelTransition, [$value_t['pageSourceId']], $value_t['pageTargetId']);
          }
          // This is the transition for the Draft to first state.
          if ($value_t['pageSourceId'] == 'draft' &&  $value_t['pageTargetId'] != 'published') {
            $workflow->getTypePlugin()->addTransition($machineTranstion, $labelTransition, [$value_t['pageSourceId']], $value_t['pageTargetId']);
          }
          // This is transition for the last state to published.
          if ($value_t['pageSourceId'] != 'draft' &&  $value_t['pageTargetId'] == 'published') {
            $workflow->getTypePlugin()->addTransition($machineTranstion, $labelTransition, [$value_t['pageSourceId']], $value_t['pageTargetId']);
          }
        }
      }
      $workflow->save();
    }
  }

  /**
   * Update workflow with states.
   *
   * @param string $name
   *   The machine name of the workflow.
   * @param array $all_new_states
   *   The states of the workflow.
   */
  public function updateWorkflowStates($name, array $all_new_states): void {
    $workflow = $this->entityTypeManager->getStorage('workflow')->load($name);
    if ($workflow) {
      // Create states.
      foreach ($all_new_states as $key => $value) {
        // Check if the state exists in the system.
        // Key is the blockID in frontend and value is the Label of the block.
        if ($workflow->getTypePlugin()->hasState($key)) {
          $workflow->getTypePlugin()->setStateLabel($key, $value);
        }
      }
      $workflow->save();
    }
  }

  /**
   * Get workflow by name.
   *
   * @param string $name
   *   The name of the workflow.
   *
   * @return array
   *   Return json and label.
   */
  public function getWorkflowDb($name): ?array {
    $data = NULL;
    $query = $this->dataBase->select('workflows_ui', 'ei');
    $query->condition('ei.workflow_machine_name', $name);
    $query->fields('ei', ['workflow_json', 'label']);
    $result = $query->execute()->fetchAll();
    if ($result) {
      $data['jsonData'] = $result[0]->workflow_json;
      $data['label'] = $result[0]->label;
    }
    return $data;
  }

  /**
   * List all workflows from the module's inventory.
   *
   * @return array
   *   Returns a list of workflows
   */
  public function getAllWorkflows(): array {
    $workflows = [];
    $query = $this->dataBase->select('workflows_ui', 'ei');
    $query->fields('ei', ['wid', 'workflow_machine_name']);
    $result = $query->execute()->fetchAll();
    foreach ($result as $workflow) {
      $workflows[] = $workflow->workflow_machine_name;
    }
    return $workflows;
  }

  /**
   * Update workflow by name.
   *
   * @param string $name
   *   The name of the workflow.
   * @param string $label
   *   The label of the wokflow.
   * @param string $json
   *   The json data.
   */
  public function updateWorkflowDb($name, $label, $json): void {
    // Remove spaces and uppercases for id.
    $id = strtolower($name);
    $id = str_replace(' ', '_', $id);
    $timestamp = time();
    $user = \Drupal::currentUser();
    // End workaround to avoid deletion error.
    $query = $this->dataBase->update('workflows_ui');
    $query->condition('workflow_machine_name', $id);
    $query->fields([
      'label' => $label,
      'workflow_json' => $json,
      'last_edited' => $timestamp,
      'last_edited_by' => $user->id(),
    ]);
    $query->execute();
  }

  /**
   * Update the info related to what entities use the workflow.
   */
  public function updateWorkflowAssignments($name, $assignments): void {
    $query = $this->dataBase->update('workflows_ui');
    $query->condition('workflow_machine_name', $name);
    $query->fields([
      'used_in' => $assignments,
    ]);
    $query->execute();
  }

  /**
   * Create workflow by name.
   *
   * @param string $name
   *   The name of the workflow.
   * @param string $label
   *   The label of the wokflow.
   * @param string $json
   *   The json data.
   * @param string $used_in
   *   Bundles that use the workflow.
   */
  public function createWorkflowDb($name, $label, $json, $used_in = ''): void {
    // Remove spaces and uppercases for id.
    $id = strtolower($name);
    $id = str_replace(' ', '_', $id);
    $user = \Drupal::currentUser();
    $timestamp = time();
    // End workaround to avoid deletion error.
    $query = $this->dataBase->insert('workflows_ui');
    $query->fields([
      'workflow_machine_name' => $id,
      'workflow_json' => $json,
      'label' => $label,
      'created' => $timestamp,
      'created_by' => $user->id(),
      'last_edited' => $timestamp,
      'last_edited_by' => $user->id(),
      'used_in' => $used_in,
    ])->execute();
  }

  /**
   * Generates the JSON definition based on states and transitions.
   *
   * @param string $states
   *   The list of workflow states.
   * @param string $transitions
   *   The list of workflow transitions.
   *
   * @return string
   *   Returns JSON data representing workflow definition.
   */
  public function generateJson($states, $transitions): string {
    $nodes = [];
    // Create the default points & states (Start, Draft, Published, End).
    $nodes['startpoint'] = [
      'blockId' => 'startpoint',
      'nodetype' => 'startpoint',
      'positionX' => 0,
      'positionY' => 350,
    ];
    $nodes['draft'] = [
      'blockId' => 'draft',
      'nodetype' => 'default',
      'label' => 'Draft',
      'positionX' => 150,
      'positionY' => 400,
    ];
    $nodes['published'] = [
      'blockId' => 'published',
      'nodetype' => 'default',
      'label' => 'Published',
      'positionX' => 1050,
      'positionY' => 400,
    ];
    $nodes['endpoint'] = [
      'blockId' => 'endpoint',
      'nodetype' => 'endpoint',
      'positionX' => 1200,
      'positionY' => 350,
    ];
    // Increments for the coordinates of the elements in the graph.
    $x = 150;
    $y = 50;
    // Add the actual workflow states from the workflow.
    foreach ($states as $state) {
      $state_id = $state->id();
      if (!in_array($state_id, ['draft', 'published'])) {
        $x += 200;
        $nodes[$state->id()] = [
          'blockId' => 'taskcontainer_' . $state->id(),
          'nodetype' => 'task',
          'label' => $state->label(),
          'positionX' => $x,
          'positionY' => 350 + $y,
        ];
        $y = -$y;
      }
    };
    $connections = [];
    // Create the default connections between the default states.
    $connections[] = [
      'connectionId' => 'con_st',
      'pageSourceId' => 'startpoint',
      'pageTargetId' => 'draft',
    ];
    $connections[] = [
      'connectionId' => 'con_en',
      'pageSourceId' => 'published',
      'pageTargetId' => 'endpoint',
    ];
    // Add the actual transitions from the workflow.
    foreach ($transitions ?: [] as $key => $transition) {
      $to = $nodes[$transition->to()->id()]['blockId'];
      foreach ($transition->from() as $trans) {
        $connections[] = [
          'connectionId' => $transition->id(),
          'pageSourceId' => $nodes[$trans->id()]['blockId'],
          'pageTargetId' => $to,
        ];
      }
    }
    // Build the array with states and transitions.
    $result = [];
    foreach ($nodes as $node) {
      $result['nodes'][] = $node;

    };
    foreach ($connections as $connection) {
      $result['connections'][] = $connection;
    };
    $result['numberOfElements'] = count($nodes) - 4;
    // Return the states and transitions as JSON data.
    return JSON::encode($result);
  }

  /**
   * Get the workflow by name.
   *
   * @param string $name
   *   The name of the workflow.
   *
   * @return bool
   *   Return the if exists.
   */
  public function getWorkflow($name): bool {
    $workflow = $this->entityTypeManager->getStorage('workflow')->load($name);
    if ($workflow) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get states of the workflow.
   *
   * @param string $name
   *   The name of the workflow.
   *
   * @return array
   *   Return the states.
   */
  public function getStatesOfWorkflow($name): array {
    $states = [];
    $workflow = $this->entityTypeManager->getStorage('workflow')->load($name);
    if ($workflow) {
      $states = $workflow->getTypePlugin()->getStates();
    }
    return $states;
  }

  /**
   * Delete states of the workflow.
   *
   * @param string $name
   *   The name of the workflow.
   * @param string $stateName
   *   The state name.
   *
   * @return array|null
   *   Return the states.
   */
  public function deleteStates($name, $stateName): void {
    $exceptions = [];
    $workflow = $this->entityTypeManager->getStorage('workflow')->load($name);
    if ($workflow) {

      try {
        $workflow->getTypePlugin()->deleteState($stateName);
        $workflow->save();
      }
      catch (\Exception $e) {
        // log error in dblog.
        \Drupal::logger('editorial_interface')->error($e->getMessage());
      }
    }
  }

  /**
   * Truncates the Workflow registry table.
   */
  public function clearWorkflowTable(): void {
    $query = $this->dataBase->truncate('workflows_ui')->execute();
  }

}
